﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    Rigidbody rb;
    public float speed;
    int count = 0;
    public Text countText;
    public Text winText;
    public Text dieText;
    public CameraController deathScript;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        countText.text = "Count: " + count.ToString();
     //   deathScript = new CameraController;
    }
	
	// Update is called once per frame
	void Update () {

	}

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            count ++;
            SetCountText();
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("Wall"))
        {
            dieText.text = "YOU DIED";
            Destroy(gameObject);
            deathScript.Death();
        }
    }


    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            winText.text = "YOU WIN!!!";
            deathScript.Death();
        }
    }
}
