﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawn : MonoBehaviour {

    public GameObject pickupPrefab;

    public float xMin, xMax, zMin, zMax;

	// Use this for initialization
	void Start () {
        for(int i = 0; i <13; i++)
        {
            SpawnPickup();
        }

        Invoke("SpawnRepeat", 5f);
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void SpawnRepeat()
    {
        SpawnPickup();
        Invoke("SpawnRepeat", 5f);
    }

    void SpawnPickup()
    {
        Vector3 randomPos = new Vector3(Random.Range(xMin, xMax), pickupPrefab.transform.position.y, Random.Range(zMin, zMax));

        Instantiate(pickupPrefab, randomPos, pickupPrefab.transform.rotation);
    }
}
